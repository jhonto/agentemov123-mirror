package com.enterdev.agentemov123;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class AgendaActivity extends AppCompatActivity {

    Toolbar toolbar;
    Button btnActivo, btnInactivo;
    LinearLayout lyData;
    ListAdapter adapter;

    Context context;
    View header;

    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private ArrayAdapter<String> mAdapter;
    private ActionBarDrawerToggle mDrawerToggle;
    private String mActivityTitle;

    int[] imagenes = new int[]{R.drawable.ic_action_add,R.drawable.ic_action_add,
            R.drawable.ic_action_add,R.drawable.ic_action_add,R.drawable.ic_action_add,
            R.drawable.ic_action_add,R.drawable.ic_action_add,R.drawable.ic_action_add};

    String[] titulo = new String[]{"Servicio 1", "Servicio 2", "Servicio 3", "Servicio 4", "Servicio 5",
            "Servicio 6", "Servicio 7", "Servicio 8"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agenda);

        toolbar = (Toolbar)findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        context = this;

        mDrawerList = (ListView) findViewById(R.id.navList);
        header = getLayoutInflater().inflate(R.layout.nav_header, null);
        mDrawerList.addHeaderView(header);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mActivityTitle = getTitle().toString();

        addDrawerItems();
        setupDrawer();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        btnActivo = (Button)findViewById(R.id.btnActivo);
        btnActivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnActivo.isEnabled()) {
                    Toast toast = Toast.makeText(getApplicationContext(), "Estado Activo", Toast.LENGTH_SHORT);
                    toast.show();
                }else {
                    btnActivo.setEnabled(false);
                    Toast toast = Toast.makeText(getApplicationContext(), "Cambie su estado", Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });

        btnInactivo = (Button)findViewById(R.id.btnInactivo);
        btnInactivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast toast = Toast.makeText(getApplicationContext(), "Estado Inactivo", Toast.LENGTH_SHORT);
                toast.show();
            }
        });

/*        lyData = (LinearLayout)findViewById(R.id.lyData);
        lyData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verDetalleAgenda();
            }
        });*/

        final ListView list = (ListView) findViewById(R.id.listView);
        adapter = new CustomAdapter(this, titulo, imagenes);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView adapterView, View view, int i, long l) {
                //Toast.makeText(getApplicationContext(), "click sobre" +i, Toast.LENGTH_SHORT).show();
                if (i == 0) {
                    Intent intent = new Intent(AgendaActivity.this, DetalleAgendaActivity.class);
                    intent.putExtra("", imagenes);
                    intent.putExtra("", titulo);
                    startActivity(intent);
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_call) {
            launchCall();
            return true;
        }
        if (id == R.id.action_services) {
            launchServices();
            return true;
        }
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void launchCall(){
        Context context = getApplicationContext();
        Toast toast = Toast.makeText(context, "lanza una llamada", Toast.LENGTH_SHORT);
        toast.show();
    }

    public void launchServices(){
        Context context = getApplicationContext();
        Toast toast = Toast.makeText(context, "lanza una lista de servicios", Toast.LENGTH_SHORT);
        toast.show();
    }

/*    public void verDetalleAgenda(){
        Intent intent = new Intent(this, DetalleAgendaActivity.class);
        startActivity(intent);
    }*/

    private void addDrawerItems() {
        String[] osArray = { "Mi Perfil", "Historial", "Agendados", "Pendientes", "Cerrar Sesión" };
        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(AgendaActivity.this, "Eligir opción", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("Menú");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

}
