package com.enterdev.agentemov123;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView txtRecuperarContrasena;
    Button btnIngresarAgenda;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtRecuperarContrasena = (TextView)findViewById(R.id.txtRecuperarContraseña);
        txtRecuperarContrasena.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recuperarContrasena();
            }
        });

        btnIngresarAgenda = (Button)findViewById(R.id.btnIngresar);
        btnIngresarAgenda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ingresoHome();
            }
        });

    }

    public void recuperarContrasena(){
        Intent intent = new Intent(this, ContrasenaActivity.class);
        startActivity(intent);
    }

    public void ingresoHome(){
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
    }

}
