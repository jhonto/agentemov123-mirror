package com.enterdev.agentemov123;

import android.app.Activity;
import android.app.Application;

/**
 * Created by PROGRAMADOR4 on 24/12/2015.
 */
public class Globals extends Application {

    private Double latitude, longitude;

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLatitude(){
        return latitude;
    }

    public void setLongitude(Double longitude){
        this.longitude = longitude;
    }

    public Double getLongitude(){
        return longitude;
    }

}
