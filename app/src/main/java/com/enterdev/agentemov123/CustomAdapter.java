package com.enterdev.agentemov123;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by PROGRAMADOR4 on 07/12/2015.
 */
public class CustomAdapter extends BaseAdapter {

    String [] titulos;
    Context context;
    int [] imagenes;
    LayoutInflater inflater;

    public CustomAdapter(Context context, String[] titulos, int[] imagenes) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.titulos = titulos;
        this.imagenes = imagenes;
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return titulos.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        TextView txtTitle;
        ImageView imgImg;

        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.list_view, parent, false);

        txtTitle = (TextView)itemView.findViewById(R.id.tv_UsuarioServicio);
        imgImg = (ImageView)itemView.findViewById(R.id.img_Item);

        return itemView;
    }

}
