package com.enterdev.agentemov123;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Toast;
import android.view.ContextMenu.ContextMenuInfo;

import java.util.ArrayList;

public class RechazarActivity extends AppCompatActivity {

    Toolbar toolbar;
    Button btnCancelarFormulario;
    Button btnAceptarFormulario;

    Context context;
    View header;

    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private ArrayAdapter<String> mAdapter;
    private ActionBarDrawerToggle mDrawerToggle;
    private String mActivityTitle;

    ArrayList<String> selection = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rechazar);

        toolbar = (Toolbar)findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        context = this;

        mDrawerList = (ListView) findViewById(R.id.navList);
        header = getLayoutInflater().inflate(R.layout.nav_header, null);
        mDrawerList.addHeaderView(header);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mActivityTitle = getTitle().toString();

        addDrawerItems();
        setupDrawer();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        btnAceptarFormulario = (Button) findViewById(R.id.btnAceptarFormulario);
        /*btnAceptarFormulario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast toast = Toast.makeText(getApplicationContext(), "Formulario Enviado", Toast.LENGTH_SHORT);
                toast.show();
            }
        });*/
        registerForContextMenu(btnAceptarFormulario);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_call) {
            launchCall();
            return true;
        }
        if (id == R.id.action_services) {
            launchServices();
            return true;
        }
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void launchCall(){
        Context context = getApplicationContext();
        Toast toast = Toast.makeText(context, "lanza una llamada", Toast.LENGTH_SHORT);
        toast.show();
    }

    public void launchServices(){
        Context context = getApplicationContext();
        Toast toast = Toast.makeText(context, "lanza una lista de servicios", Toast.LENGTH_SHORT);
        toast.show();
    }

    public void selectItems(View view){
        boolean checked = ((CheckBox) view).isChecked();

        switch (view.getId()){
            case R.id.option_1:
                if (checked){
                    selection.add("option 1");
                }else {
                    selection.remove("option 1");
                }
                break;
            case R.id.option_2:
                if (checked){
                    selection.add("option 2");
                }else {
                    selection.remove("option 2");
                }
                break;
            case R.id.option_3:
                if (checked){
                    selection.add("option 3");
                }else {
                    selection.remove("option 3");
                }
        }

    }

    private void addDrawerItems() {
        String[] osArray = { "Mi Perfil", "Historial", "Agendados", "Pendientes", "Cerrar Sesión" };
        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(RechazarActivity.this, "Eligir opción", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("Menú");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

            MenuInflater inflater = getMenuInflater();
            menu.setHeaderTitle("Esta Seguro?");
            menu.setHeaderIcon(R.drawable.cast_ic_notification_0);
            inflater.inflate(R.menu.type_services_menu, menu);

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        super.onContextItemSelected(item);

        switch (item.getItemId()){
            case R.id.id_si:
                Toast toast = Toast.makeText(this, "Se envio el formulario", Toast.LENGTH_SHORT);
                toast.show();
                return true;
            case R.id.id_no:
                Toast toast1 = Toast.makeText(this, "Se cancelo el envio", Toast.LENGTH_SHORT);
                toast1.show();
                return true;
            default:
                return false;
        }
    }

}
