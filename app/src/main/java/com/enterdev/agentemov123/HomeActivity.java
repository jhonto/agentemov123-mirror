package com.enterdev.agentemov123;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SimpleTimeZone;

public class HomeActivity extends AppCompatActivity {


    private GoogleMap mMap;

    Context context;

    Button btnActivo;
    Button btnInactivo;

    TextView locationText;
    TextView addressText;
    TextView longitudeText;
    TextView latitudeText;
    TextView tiempoText;
    TextView rangoText;

    private static final String REGISTER_URL = "http://www.siont.com.co/clickping/update.php";

    public static  final String KEY_LONGITUDE = "longitude";
    public static final String KEY_LATITUDE = "latitude";
    public static final String KEY_TIEMPO = "tiempo";
    public static final String KEY_RANGO = "rango";

    Double longitude, latitude;

    Float distance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        /*SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_HomeLocation);
        mapFragment.getMapAsync(this);*/

//        setUpMapIfNeeded();

        context = this;

        locationText = (TextView) findViewById(R.id.location);
        addressText = (TextView) findViewById(R.id.address);
        longitudeText = (TextView) findViewById(R.id.longitude);
        latitudeText = (TextView) findViewById(R.id.latitude);
        tiempoText = (TextView) findViewById(R.id.tiempo);
        rangoText = (TextView )findViewById(R.id.rango);

        btnActivo = (Button)findViewById(R.id.btn_AgenteActivo);
        btnActivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateLocation();
            }
        });

        btnInactivo = (Button)findViewById(R.id.btn_AgenteInactivo);
        btnInactivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        replaceMapFragment();

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */

/*    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }*/

/*    @Override
    protected void onResume(){
        super.onResume();
        setUpMapIfNeeded();
    }

    public void onSearch(View view){
        Double latLocation = 6.217;
        Double lonLocation = -75.567;
        List<Address> addressList = null;
        String location = String.valueOf(latLocation + lonLocation).toString();

        if(location != null || !location.equals("") )
        {
            Geocoder geocoder = new Geocoder(this);
            try {
                addressList = geocoder.getFromLocationName(location, 1);
            }catch (IOException e){
                e.printStackTrace();
            }
            Address address = addressList.get(0);
            LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
            mMap.addMarker(new MarkerOptions().position(latLng).title("Marker"));
            mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        }

    }*/

/*    private void setUpMapIfNeeded(){
        if (mMap == null){

            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_HomeLocation))
                    .getMap();

            if (mMap != null){
                setUpMap();
            }
        }
    }

    private void setUpMap(){
        mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
        mMap.setMyLocationEnabled(true);
    }*/

    private void replaceMapFragment() {
        mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_HomeLocation))
                .getMap();

        // Enable Zoom
        mMap.getUiSettings().setZoomGesturesEnabled(true);

        //set Map TYPE
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //enable Current location Button
        mMap.setMyLocationEnabled(true);

        //set "listener" for changing my location
        mMap.setOnMyLocationChangeListener(myLocationChangeListener());
    }

    private GoogleMap.OnMyLocationChangeListener myLocationChangeListener(){
        return new GoogleMap.OnMyLocationChangeListener(){
            @Override
            public void onMyLocationChange(Location location){
                LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
                longitude = location.getLongitude();
                latitude = location.getLatitude();
                longitudeText.setText(longitude.toString());
                latitudeText.setText(latitude.toString());

                DateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date date = new Date(location.getTime());
                String formatted = format.format(date);
                tiempoText.setText(formatted.toString());

                Location currentlocation = new Location("dentro del rango");
                currentlocation.setLongitude(longitude);
                currentlocation.setLatitude(latitude);

                Location newLocation = new Location("fuera del rango");
                newLocation.setLongitude(longitude);
                newLocation.setLatitude(latitude);


                distance = currentlocation.distanceTo(newLocation);
                rangoText.setText(distance.toString());

                mMap.addMarker(new MarkerOptions().position(loc));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 16.0f));
                locationText.setText("estas en [" + longitude + " ; " + latitude + " ; " + distance + "] ");

                //get current address by invoke an AsyncTask object
                new GetAddressTask(HomeActivity.this).execute(String.valueOf(latitude), String.valueOf(longitude));
            }
        };
    }

    public void callBackDataFromAsyncTask(String address) {
        addressText.setText(address);
    }

    private void updateLocation(){

        final String longitude = longitudeText.getText().toString().trim();
        final String latitude = latitudeText.getText().toString().trim();
        final String tiempo = tiempoText.getText().toString().trim();
        final String rango = rangoText.getText().toString().trim();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(HomeActivity.this,response,Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(HomeActivity.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put(KEY_LONGITUDE, longitude);
                params.put(KEY_LATITUDE, latitude);
                params.put(KEY_TIEMPO, tiempo);
                params.put(KEY_RANGO, rango);
                return params;
            }

        };
        if(longitude ==  longitude) {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_call) {
            launchCall();
            return true;
        }
        if (id == R.id.action_services) {
            launchServices();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void launchCall(){
        Context context = getApplicationContext();
        Toast toast = Toast.makeText(context, "lanza una llamada", Toast.LENGTH_SHORT);
        toast.show();
    }

    public void launchServices(){
        Context context = getApplicationContext();
        Toast toast = Toast.makeText(context, "lanza una lista de servicios", Toast.LENGTH_SHORT);
        toast.show();
    }

}
