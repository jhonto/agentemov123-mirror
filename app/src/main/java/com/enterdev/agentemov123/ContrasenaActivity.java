package com.enterdev.agentemov123;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class ContrasenaActivity extends AppCompatActivity {

    Toolbar toolbar;
    Button btnRecuperarContrasena;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contrasena);

        toolbar = (Toolbar)findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        btnRecuperarContrasena = (Button)findViewById(R.id.btnRecuperarContrasena);
        btnRecuperarContrasena.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast toast = Toast.makeText(getApplicationContext(), "Pronto recibira un correo con su contraseña", Toast.LENGTH_SHORT);
                toast.show();
            }
        });

    }

    public boolean onCreateOptionsMenu(Menu menu){

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.action_call:
                launchCall();
                return true;
            case R.id.action_services:
                launchServices();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void launchCall(){
        Context context = getApplicationContext();
        Toast toast = Toast.makeText(context, "lanza una llamada", Toast.LENGTH_SHORT);
        toast.show();
    }

    public void launchServices(){
        Context context = getApplicationContext();
        Toast toast = Toast.makeText(context, "lanza una lista de servicios", Toast.LENGTH_SHORT);
        toast.show();
    }

}
